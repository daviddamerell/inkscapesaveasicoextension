#!/usr/bin/perl -w
# Copyright David R. Damerell (david@nixbioinf.org)
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
# the License. You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
# an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
# specific language governing permissions and limitations under the License

use strict;
use warnings;

if(@ARGV!=1){
	die "Usage\tInput PNG file\n";
}

my $inputPngFile=$ARGV[0];

my @fileList;

generatePPM($inputPngFile, 'false');
generatePPM($inputPngFile, 'true');

push(@fileList, "$inputPngFile.ppm");
push(@fileList, "$inputPngFile.pgn");

foreach my $width ((36,28,24,16,8)){
	my $nextPngFile="$inputPngFile.$width";

        resizeImage($inputPngFile, $nextPngFile, $width);

	generatePPM($nextPngFile, 'false');
	generatePPM($nextPngFile, 'true');

	push(@fileList, "$nextPngFile.ppm");
	push(@fileList, "$nextPngFile.pgn");
}

convertToIco(\@fileList, "$inputPngFile.ico");

foreach my $fileToDelete (@fileList){
	unlink($fileToDelete);
}

my $catCommand="cat $inputPngFile.ico";
system($catCommand);
if($? >> 8 !=0){
	die "Unable to run command: $catCommand\n";
}

unlink("$inputPngFile.ico");

sub convertToIco{
	my @fileList=@{$_[0]};
	my $outputFileName=$_[1];

	my $icoCommand="ppmtowinicon -andpgms -output $outputFileName ";
	$icoCommand.=join(' ',@fileList);

	system("$icoCommand 2>/dev/null");
	if($? >>8 !=0){
		die "Unable to run command: $icoCommand\n";
	}
}


sub resizeImage{
	my $pngToResize=$_[0];
	my $resizedPngFile=$_[1];
	my $newWidth=$_[2];
	
	my $command="convert -colors 256 -resize $newWidth $pngToResize $resizedPngFile 2> /dev/null";

	system($command);
	if($? >> 8 !=0){
		die "Unable to run command $command\n";
	}
}

sub generatePPM{
	my $pngFile=$_[0];
	my $alphaChannel=$_[1];

	my $options=$alphaChannel eq 'true' ? '-alpha' : '';
        my $extension=$alphaChannel eq 'true' ? 'pgn' : 'ppm';

	my $command="pngtopnm $options $pngFile 1> $pngFile.$extension 2> /dev/null";

	system($command);
	if($? >>8 !=0){
		die "Unable to run command: $command\n";
	}

	if($alphaChannel eq 'true'){
		my $colourLimitCommand="pnmquant 256 $pngFile.ppm 1> $pngFile.256.ppm 2> /dev/null";
		system($colourLimitCommand);
		if($? >> 8 !=0){
			die "Unable to run command: $colourLimitCommand";
		}

		`mv $pngFile.256.ppm $pngFile.ppm`;
	}
}
