#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright David R. Damerell (david@nixbioinf.org)
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
# the License. You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
# an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
# specific language governing permissions and limitations under the License

import os
import sys
import subprocess

if len(sys.argv)!=2:
  sys.exit("Usage\tInput PNG file\n")

inputPngFile=sys.argv[1]

fileList=[]

def generatePPM(pngFile, alphaChannel):
  options='-alpha' if alphaChannel=='true' else ''
  extension='pgn' if alphaChannel=='true' else 'ppm'

  command="pngtopnm " +options+ " "+ pngFile+ "  1> "+ pngFile+ "."+extension+" 2> /dev/null"
  
  retVal=subprocess.check_call(command,shell=True)
  if retVal!=0:
    sys.exit("Unable to run command: "+command)
    
  if alphaChannel=='true':  
    colourLimitCommand="pnmquant 256 "+pngFile+".ppm 1> "+pngFile+".256.ppm 2> /dev/null"
    retVal=subprocess.check_call(colourLimitCommand, shell=True)
    
    if retVal!=0:
      sys.exit("Unable to run command: "+colourLimitCommand)
      
    subprocess.check_call("mv "+pngFile+".256.ppm "+pngFile+".ppm", shell=True)  

def resizeImage(pngToResize, resizedPngFile, newWidth):
  command="convert -colors 256 -resize "+str(newWidth)+" "+pngToResize+" "+resizedPngFile+" 2> /dev/null";
  retVal=subprocess.check_call(command, shell=True)
  if retVal!=0:
    sys.exit("Unable to run command: "+command)
    
def convertToIco(fileList, outputFileName):
  icoCommand="ppmtowinicon -andpgms -output "+outputFileName+" "
  icoCommand+=' '.join(fileList)
    
  retVal=subprocess.check_call(icoCommand+" 2> /dev/null", shell=True)
  if retVal!=0:
    sys.exit("Unable to run command: "+icoCommand)


generatePPM(inputPngFile, 'false')
generatePPM(inputPngFile, 'true');

fileList.append(inputPngFile+".ppm")
fileList.append(inputPngFile+".pgn")

for width in [36,28,24,16,8]:
  nextPngFile=inputPngFile+"."+str(width)

  resizeImage(inputPngFile, nextPngFile, width);

  generatePPM(nextPngFile, 'false')
  generatePPM(nextPngFile, 'true')

  fileList.append(nextPngFile+".ppm")
  fileList.append(nextPngFile+".pgn")

convertToIco(fileList, inputPngFile+".ico");

for fileToDelete in fileList:
  os.remove(fileToDelete)

catCommand="cat "+inputPngFile+".ico"
retVal=subprocess.check_call(catCommand,shell=True)
if retVal!=0:
  sys.exit("Unable to run command: "+catCommand+"\n")

os.remove(inputPngFile+".ico")  
  
  
  
  
  
  
